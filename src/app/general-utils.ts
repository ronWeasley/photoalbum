export function stringContainsString(firstString: string, secondString: string): boolean {
  return firstString.toLocaleLowerCase().includes(secondString.toLocaleLowerCase());
}
