import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RowPhotoViewComponent } from './row-photo-view.component';

describe('RowPhotoViewComponent', () => {
  let component: RowPhotoViewComponent;
  let fixture: ComponentFixture<RowPhotoViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RowPhotoViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RowPhotoViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
