import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { ColumnPhotoViewComponent } from '../photo-view/column-photo-view/column-photo-view.component';
import { RowPhotoViewComponent } from './row-photo-view/row-photo-view.component';

@NgModule({
  declarations: [ColumnPhotoViewComponent, RowPhotoViewComponent],
  exports: [ColumnPhotoViewComponent, RowPhotoViewComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
  ],
})
export class PhotoViewModule {}
