import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PhotoView } from 'src/app/models/photo-view.model';

@Component({
  selector: 'app-column-photo-view',
  templateUrl: './column-photo-view.component.html',
  styleUrls: ['./column-photo-view.component.scss'],
})
export class ColumnPhotoViewComponent implements OnInit {
  @Input() photoView: PhotoView;

  @Output() photoClick: EventEmitter<string> = new EventEmitter<string>();
  @Output() photoDelete: EventEmitter<string> = new EventEmitter<string>();

  imgUrl: string;

  constructor() {}

  ngOnInit(): void {
    this.imgUrl = this.photoView.imgUrl ? this.photoView.imgUrl : 'assets/photos/no_image_available.png';
  }

  onPhotoClick(): void {
    this.photoClick.next(this.photoView.id);
  }

  onClickPhotoDelete(): void {
    this.photoDelete.next(this.photoView.id);
  }
}
