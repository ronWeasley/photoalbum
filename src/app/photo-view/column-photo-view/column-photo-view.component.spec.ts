import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnPhotoViewComponent } from './column-photo-view.component';

describe('ColumnPhotoViewComponent', () => {
  let component: ColumnPhotoViewComponent;
  let fixture: ComponentFixture<ColumnPhotoViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColumnPhotoViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnPhotoViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
