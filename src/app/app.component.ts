import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor() {
    document.getElementById('favicon-link').setAttribute('href', 'photo-album-favicon.ico');
  }

  title = 'photo-album';
}
