import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConfirmationDialogService } from '../common-layout/confirmation-dialog/confirmation-dialog.service';
import { stringContainsString } from '../general-utils';
import { PhotoView } from '../models/photo-view.model';
import { Photo } from '../models/photo.model';
import { PhotosDialogComponent } from './photos-dialog/photos-dialog.component';
import { DeletePhoto, LoadPhotosForAlbum } from './state/photos.actions';
import { PhotosState } from './state/photos.state';

@Component({
  selector: 'app-photos-page',
  templateUrl: './photos-page.component.html',
  styleUrls: ['./photos-page.component.scss'],
})
export class PhotosPageComponent implements OnInit {
  photos$: Observable<Array<Photo>>;
  listView: boolean;
  searchValue: string;
  page = 1;

  private photoDialogRef: MatDialogRef<PhotosDialogComponent>;

  constructor(private store: Store, private confirmationDialog: ConfirmationDialogService, public dialog: MatDialog) {}

  ngOnInit(): void {
    this.installScrollEvent();
    this.photos$ = this.store.select((state) => state.photos.photos);
  }

  onClickGridView(): void {
    this.listView = false;
  }

  onClickListView(): void {
    this.listView = true;
  }

  onSearchChange(searchText: string): void {
    this.photos$ = this.store.select((state) => state.photos.photos);
    this.photos$ = this.photos$.pipe(map((photos) => photos.filter((photo) => stringContainsString(photo.title, searchText))));
  }

  onClickBack(): void {
    this.store.dispatch(new Navigate(['/albums']));
  }

  getPhotoViewForAlbum(photo: Photo): PhotoView {
    return { title: photo.title, id: photo.id, isAlbumView: false, imgUrl: photo.url };
  }

  onPhotoClick(photoId: string): void {
    this.photoDialogRef = this.dialog.open(PhotosDialogComponent, {
      disableClose: false,
      width: '100vw',
      maxWidth: '100vw',
      height: '100vh',
    });

    this.photoDialogRef.componentInstance.photoId = photoId;
    this.photoDialogRef.afterClosed().subscribe((result) => {
      this.photoDialogRef = null;
    });
  }

  onPhotoDelete(photoId: string): void {
    this.confirmationDialog.show({
      title: 'Confirm',
      message: 'Are you sure you want to remove photo?',
    });

    this.confirmationDialog.closed().subscribe((confirmed) => {
      if (confirmed) {
        this.store.dispatch(new DeletePhoto(photoId));
      }
    });
  }

  private installScrollEvent(): void {
    const albumsPageHolder = document.getElementById('photos-holder');
    albumsPageHolder.addEventListener('scroll', () => {
      const bottomScrollHeight = 14;
      if (albumsPageHolder.offsetHeight + albumsPageHolder.scrollTop + bottomScrollHeight >= albumsPageHolder.scrollHeight) {
        const albumId = this.store.selectSnapshot(PhotosState.getAlbumId);
        this.store.dispatch(new LoadPhotosForAlbum(albumId, this.page++));
      }
    });
  }
}
