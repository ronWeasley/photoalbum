import { Photo } from 'src/app/models/photo.model';

export interface PhotosStateModel {
  photos: Array<Photo>;
  searchText: string;
}

export function initialPhotosState(): PhotosStateModel {
  return {
    photos: [],
    searchText: ''
  };
}
