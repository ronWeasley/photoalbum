export class LoadPhotosForAlbum {
  static readonly type = '[AlbumsPage] loadPhotosForAlbum';

  constructor(public albumId: string, public page: number, public size: number = 10) {}
}

export class DeletePhoto {
  static readonly type = '[PhotosPage] deletePhoto';

  constructor(public photoId: string) {}
}
