import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PhotosService {
  constructor() {}

  loadPhotosForAlbum(albumId: string, page: number, size: number = 10): Promise<Response> {
    return fetch(`https://jsonplaceholder.typicode.com/photos?albumId=${albumId}&_page=${page}&_limit=${size}`);
  }

  deletePhoto(photoId: string): Promise<Response> {
    return fetch(`https://jsonplaceholder.typicode.com/photos?id=${photoId}`, { method: 'DELETE' });
  }
}
