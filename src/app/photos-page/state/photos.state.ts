import { Injectable } from '@angular/core';
import { Navigate } from '@ngxs/router-plugin';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { SpinnerOverlayService } from 'src/app/common-layout/spinner-overlay/spinner-overlay.service';
import { stringContainsString } from 'src/app/general-utils';
import { Photo } from 'src/app/models/photo.model';
import { DeletePhoto, LoadPhotosForAlbum } from './photos.actions';
import { PhotosService } from './photos.service';
import { initialPhotosState, PhotosStateModel } from './photos.state.model';

@State<PhotosStateModel>({
  name: 'photos',
  defaults: initialPhotosState(),
})
@Injectable()
export class PhotosState {
  @Selector()
  static getPhotos(state: PhotosStateModel): Array<Photo> {
    return state.photos;
  }

  @Selector()
  static getAlbumId(state: PhotosStateModel): string {
    return state.photos[0].albumId;
  }

  @Selector()
  static searchPhotos(state: PhotosStateModel) {
    return state.photos.filter((photo) => {
      return stringContainsString(photo.title, state.searchText);
    });
  }

  constructor(private photosService: PhotosService, private spinner: SpinnerOverlayService) {}

  @Action(LoadPhotosForAlbum)
  loadPhotosForAlbum(ctx: StateContext<PhotosStateModel>, { albumId, page }: LoadPhotosForAlbum) {
    this.spinner.show('Loading photos...');
    this.photosService
      .loadPhotosForAlbum(albumId, page)
      .then((response) => response.json())
      .then((photos: Array<Photo>) => {
        this.spinner.hide();
        const newPhotos = ctx.getState().photos.concat(photos);
        ctx.patchState({
          photos: newPhotos,
        });
        ctx.dispatch(new Navigate(['/photos']));
      });
  }

  @Action(DeletePhoto)
  deletePhoto(ctx: StateContext<PhotosStateModel>, { photoId }: DeletePhoto) {
    this.spinner.show('Deleting photo...');
    this.photosService
      .deletePhoto(photoId)
      .then((response) => response.json())
      .then(() => {
        this.spinner.hide();
      });
  }
}
