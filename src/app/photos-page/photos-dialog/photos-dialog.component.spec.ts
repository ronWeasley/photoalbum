import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotosDialogComponent } from './photos-dialog.component';

describe('PhotosDialogComponent', () => {
  let component: PhotosDialogComponent;
  let fixture: ComponentFixture<PhotosDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotosDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotosDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
