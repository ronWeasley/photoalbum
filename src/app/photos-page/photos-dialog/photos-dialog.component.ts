import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngxs/store';
import { Photo } from 'src/app/models/photo.model';
import { PhotosState } from '../state/photos.state';

@Component({
  selector: 'app-photos-dialog',
  templateUrl: './photos-dialog.component.html',
  styleUrls: ['./photos-dialog.component.scss'],
})
export class PhotosDialogComponent implements OnInit {
  photoId: string;

  photos: Array<Photo>;
  photoIndex: number;
  imageUrl: string;

  constructor(public dialogRef: MatDialogRef<PhotosDialogComponent>, private store: Store) {}

  ngOnInit(): void {
    this.photos = this.store.selectSnapshot(PhotosState.getPhotos);
    this.photoIndex = this.getCurrentImageIndex();
    this.changeImageUrl();
  }

  onClickClose(): void {
    this.dialogRef.close();
  }

  onPreviousImage(): void {
    this.photoIndex--;
    if (this.photoIndex < 0) {
      this.photoIndex = this.photos.length - 1;
    }
    this.changeImageUrl();
  }

  onNextImage(): void {
    this.photoIndex++;
    if (this.photoIndex === this.photos.length) {
      this.photoIndex = 0;
    }
    this.changeImageUrl();
  }

  private getCurrentImageIndex(): number {
    return this.photos.findIndex((photo) => photo.id === this.photoId);
  }

  private changeImageUrl(): void {
    this.imageUrl = this.photos[this.photoIndex].url;
  }
}
