import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonLayoutModule } from '../common-layout/common-layout.module';
import { ConfirmationDialogModule } from '../common-layout/confirmation-dialog/confirmation-dialog.module';
import { PhotoViewModule } from '../photo-view/photo-view.module';
import { PhotosDialogComponent } from './photos-dialog/photos-dialog.component';
import { PhotosPageComponent } from './photos-page.component';

const routes: Routes = [
  {
    path: '',
    component: PhotosPageComponent,
  },
];

@NgModule({
  declarations: [PhotosPageComponent, PhotosDialogComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatInputModule,
    PhotoViewModule,
    CommonLayoutModule,
    ConfirmationDialogModule,
  ],
})
export class PhotosPageModule {}
