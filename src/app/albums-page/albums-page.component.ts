import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Album } from '../models/album.model';
import { PhotoView } from '../models/photo-view.model';
import { LoadPhotosForAlbum } from '../photos-page/state/photos.actions';
import { LoadAlbums } from './state/albums.actions';
import { AlbumsState } from './state/albums.state';

@Component({
  selector: 'app-albums-page',
  templateUrl: './albums-page.component.html',
  styleUrls: ['./albums-page.component.scss'],
})
export class AlbumsPageComponent implements OnInit {
  albums$: Observable<Array<Album>>;
  listView: boolean;
  page = 0;

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.installScrollEvent();
    this.store.dispatch(new LoadAlbums(this.page++));
    this.albums$ = this.store.select((state) => state.albums.albums);
  }

  onClickGridView(): void {
    this.listView = false;
  }

  onClickListView(): void {
    this.listView = true;
  }

  onPhotoClick(albumId: string): void {
    this.store.dispatch(new LoadPhotosForAlbum(albumId, 0));
  }

  getPhotoViewForAlbum(album: Album): PhotoView {
    return { title: album.title, id: album.id, isAlbumView: true, creator: this.getCreatorOfAlbum(album.userId) };
  }

  private getCreatorOfAlbum(creatorId: string): string {
    const users = this.store.selectSnapshot(AlbumsState.getUsers);
    const creator = users.find((user) => user.id === creatorId).username;
    return creator;
  }

  private installScrollEvent(): void {
    const albumsPageHolder = document.getElementById('photo-albums-holder');
    albumsPageHolder.addEventListener('scroll', () => {
      const bottomScrollHeight = 14;
      if (albumsPageHolder.offsetHeight + albumsPageHolder.scrollTop + bottomScrollHeight >= albumsPageHolder.scrollHeight) {
        this.store.dispatch(new LoadAlbums(this.page++));
      }
    });
  }
}
