import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AlbumsService {
  constructor() {}

  loadAlbums(page: number, size: number = 10): Promise<Response> {
    return fetch(`https://jsonplaceholder.typicode.com/albums?_page=${page}&_limit=${size}`);
  }

  loadUsers(): Promise<Response> {
    return fetch('https://jsonplaceholder.typicode.com/users');
  }
}
