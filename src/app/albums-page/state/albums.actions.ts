export class LoadAlbums {
  static readonly type = '[AlbumsPage] LoadAlbums';

  constructor(public page: number, public size?: number) {}
}

export class LoadUsers {
  static readonly type = '[AlbumsPage] LoadUsers';
}
