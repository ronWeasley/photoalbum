import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { SpinnerOverlayService } from 'src/app/common-layout/spinner-overlay/spinner-overlay.service';
import { Album } from 'src/app/models/album.model';
import { User } from 'src/app/models/user.model';
import { LoadAlbums, LoadUsers } from './albums.actions';
import { AlbumsService } from './albums.service';
import { AlbumsStateModel, initialAlbumsState } from './albums.state.model';

@State<AlbumsStateModel>({
  name: 'albums',
  defaults: initialAlbumsState(),
})
@Injectable()
export class AlbumsState {
  @Selector()
  static getUsers(state: AlbumsStateModel): Array<User> {
    return state.users;
  }

  constructor(private albumsService: AlbumsService, private spinner: SpinnerOverlayService) {}

  @Action(LoadAlbums)
  loadAlbums(ctx: StateContext<AlbumsStateModel>, { page, size }: LoadAlbums) {
    this.spinner.show('Loading albums...');
    this.albumsService
      .loadAlbums(page)
      .then((response) => response.json())
      .then((albums: Array<Album>) => {
        this.spinner.hide();
        const newAlbums = ctx.getState().albums.concat(albums);
        ctx.patchState({
          albums: newAlbums,
        });
      });
  }

  @Action(LoadUsers)
  loadUsers(ctx: StateContext<AlbumsStateModel>) {
    this.spinner.show('Loading...');
    this.albumsService
      .loadUsers()
      .then((response) => response.json())
      .then((users: Array<User>) => {
        this.spinner.hide();
        ctx.patchState({
          users,
        });
      });
  }
}
