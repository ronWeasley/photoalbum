import { Album } from 'src/app/models/album.model';
import { User } from 'src/app/models/user.model';

export interface AlbumsStateModel {
  albums: Array<Album>;
  users: Array<User>;
}

export function initialAlbumsState(): AlbumsStateModel {
  return {
    albums: [],
    users: [],
  };
}
