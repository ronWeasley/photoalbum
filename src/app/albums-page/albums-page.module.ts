import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonLayoutModule } from '../common-layout/common-layout.module';
import { PhotoViewModule } from '../photo-view/photo-view.module';
import { AlbumsPageComponent } from './albums-page.component';

const routes: Routes = [
  {
    path: '',
    component: AlbumsPageComponent,
  },
];

@NgModule({
  declarations: [AlbumsPageComponent],
  imports: [CommonModule, RouterModule.forChild(routes), PhotoViewModule, CommonLayoutModule],
})
export class AlbumsPageModule {}
