export interface PhotoView {
  title: string;
  id: string;
  isAlbumView: boolean;
  imgUrl?: string;
  creator?: string;
}
