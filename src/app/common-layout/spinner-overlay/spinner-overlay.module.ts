import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SpinnerOverlayComponent } from './spinner-overlay.component';


@NgModule({
  declarations: [SpinnerOverlayComponent],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatCardModule
  ],
})
export class SpinnerOverlayModule {
}
