import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { Injectable, NgZone } from '@angular/core';
import { SpinnerOverlayComponent } from './spinner-overlay.component';

@Injectable({
  providedIn: 'root',
})
export class SpinnerOverlayService {
  private readonly DEFAULT_MSG = 'Loading...';
  private overlayRef: OverlayRef;

  constructor(private overlay: Overlay, private ngZone: NgZone) {}

  show(message?: string): void {
    // overlay ref is Portal host
    if (!this.overlayRef) {
      this.overlayRef = this.overlay.create();
    }
    if (this.overlayRef.hasAttached()) {
      this.overlayRef.detach();
    }
    this.ngZone.run(() => {
      // attach component portal to portal host
      const spinnerPortal = new ComponentPortal(SpinnerOverlayComponent);
      const component = this.overlayRef.attach(spinnerPortal);

      if (!message) {
        message = this.DEFAULT_MSG;
      }

      component.instance.message = message;
    });
  }

  hide(): void {
    this.ngZone.run(() => {
      if (!!this.overlayRef) {
        this.overlayRef.detach();
      }
    });
  }
}
