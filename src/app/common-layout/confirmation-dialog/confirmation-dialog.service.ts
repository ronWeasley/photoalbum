import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { ConfirmationDialogComponent } from './confirmation-dialog.component';
import { ConfirmationDialogData } from './confirmation-dialog.data';

@Injectable({
  providedIn: 'root',
})
export class ConfirmationDialogService {
  private dialogRef: MatDialogRef<ConfirmationDialogComponent>;

  constructor(private dialog: MatDialog) {}

  show(dialogData: ConfirmationDialogData): void {
    this.dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        title: dialogData.title,
        message: dialogData.message,
      },
    });
  }

  closed(): Observable<any> {
    return this.dialogRef.afterClosed().pipe(
      take(1),
      map((res) => {
        return res;
      })
    );
  }
}
