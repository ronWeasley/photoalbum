import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogData } from './confirmation-dialog.data';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss'],
})
export class ConfirmationDialogComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: ConfirmationDialogData, private dialogRef: MatDialogRef<ConfirmationDialogComponent>) {}

  ngOnInit() {}

  onClickNo(): void {
    this.dialogRef.close(false);
  }

  onClickYes(): void {
    this.dialogRef.close(true);
  }
}
