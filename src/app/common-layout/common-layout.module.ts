import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { SpinnerOverlayModule } from './spinner-overlay/spinner-overlay.module';

const modules = [
  MatButtonModule,
  MatIconModule,
  FormsModule,
  ReactiveFormsModule,
  MatFormFieldModule,
  MatInputModule,
  SpinnerOverlayModule
];

@NgModule({
  declarations: [],
  imports: [...modules],
  exports: [...modules],
})
export class CommonLayoutModule {}
