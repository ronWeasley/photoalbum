import { Injectable } from '@angular/core';
import { CanActivate, CanLoad } from '@angular/router';
import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';
import { AuthState } from '../state/auth.state';

@Injectable({
  providedIn: 'root',
})
export class LoggedInGuard implements CanActivate, CanLoad {
  constructor(private store: Store) {}

  canActivate(): boolean {
    return this.isLoggedin();
  }

  canLoad(): boolean {
    return this.isLoggedin();
  }

  private isLoggedin(): boolean {
    const isLoggedin = this.store.selectSnapshot(AuthState.isLoggedin);
    if (!isLoggedin) {
      this.store.dispatch(new Navigate(['']));
    }
    return isLoggedin;
  }
}
