import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AuthState } from './auth.state';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private store: Store) {}

  login(username: string, email: string): Promise<Response> {
    return fetch(`https://jsonplaceholder.typicode.com/users?username=${username}&email=${email}`);
  }
}
