import { User } from 'src/app/models/user.model';

export interface AuthStateModel {
  user: User;
  error?: string;
}

export function initialAuthState(): AuthStateModel {
  return {
    user: null,
  };
}
