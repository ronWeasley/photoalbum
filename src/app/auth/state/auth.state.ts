import { Injectable } from '@angular/core';
import { Navigate } from '@ngxs/router-plugin';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { LoadUsers } from 'src/app/albums-page/state/albums.actions';
import { SpinnerOverlayService } from 'src/app/common-layout/spinner-overlay/spinner-overlay.service';
import { User } from 'src/app/models/user.model';
import { Login } from './auth.actions';
import { AuthService } from './auth.service';
import { AuthStateModel, initialAuthState } from './auth.state.model';

@State<AuthStateModel>({
  name: 'auth',
  defaults: initialAuthState(),
})
@Injectable()
export class AuthState {
  @Selector()
  static isLoggedin(state: AuthStateModel): boolean {
    return !!state.user.loggedIn;
  }

  constructor(private authService: AuthService, private spinner: SpinnerOverlayService) {}

  @Action(Login)
  onLogin(ctx: StateContext<AuthStateModel>, { username, email }: Login) {
    this.spinner.show('Logging in...');
    this.authService
      .login(username, email)
      .then((response) => response.json())
      .then((users: Array<User>) => {
        this.spinner.hide();
        if (users === null || users === undefined || users.length === 0) {
          ctx.patchState({
            user: null,
            error: 'Bad credentials. Please login again.',
          });
          return;
        }
        const user = users[0];
        user.loggedIn = true;
        ctx.patchState({
          user,
        });
        ctx.dispatch(new LoadUsers());
        ctx.dispatch(new Navigate(['/albums']));
      });
  }
}
