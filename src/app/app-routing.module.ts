import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoggedInGuard } from './auth/guards/isloggedin.guard';

const routes: Routes = [
  { path: '', redirectTo: 'authorization', pathMatch: 'full' },
  {
    path: 'authorization',
    loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: 'albums',
    loadChildren: () => import('./albums-page/albums-page.module').then((m) => m.AlbumsPageModule),
    canLoad: [LoggedInGuard],
  },
  {
    path: 'photos',
    loadChildren: () => import('./photos-page/photos-page.module').then((m) => m.PhotosPageModule),
    canLoad: [LoggedInGuard],
  },
  // TODO WILDCARD PAGE
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
